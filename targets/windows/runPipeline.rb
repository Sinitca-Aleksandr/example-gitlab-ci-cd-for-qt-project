
# This script is used to generate Windows application windows app installer
#--------------------------------------------------------------------------------------------------
# As ruby does not allow to add any path to system PATH, script demands same path two times for mingw1120_64;
# one for adding to system PATH, one for to be able to call 'mingw32_make' tool
#--------------------------------------------------------
# Environment Variables:
#--------------------------------------------------------
#FOR MINGW COMPILER:
#Powershell: 
#	$env:Path=\"C:/Qt/Tools/mingw1120_64/bin;$env:Path\"
#	$env:MINGW_BIN_DIR=\"C:/Qt/Tools/mingw1120_64/bin\"
#	$env:CMAKE_BIN_PATH=\"C:/Qt/Tools/CMake_64/bin\"
#	$env:CMAKE_PREFIX_PATH=\"C:/Qt/6.3.0/mingw_64\"
#	$env:QTIF_BIN_DIR=\"C:/Qt/Tools/QtInstallerFramework/4.x/bin\"
#Cmd:
#	set PATH=C:/Qt/Tools/mingw1120_64/bin;%PATH%
#	set MINGW_BIN_DIR=C:/Qt/Tools/mingw1120_64/bin
#	set CMAKE_BIN_PATH=C:/Qt/Tools/CMake_64/bin
#	set CMAKE_PREFIX_PATH=C:/Qt/6.3.0/mingw_64
#	set QTIF_BIN_DIR=C:/Qt/Tools/QtInstallerFramework/4.x/bin
#--------------------------------------------------------
#FOR MSVC COMPILER:
#	$env:CMAKE_PREFIX_PATH=\"C:/Qt/6.3.0/msvc2019_64\" 
#	$env:CMAKE_BIN_PATH=\"C:/Qt/Tools/CMake_64/bin\"
#	$env:QTIF_BIN_DIR="+"\"C:/Qt/Tools/QtInstallerFramework/4.x/bin\"
#Cmd:
#	set CMAKE_PREFIX_PATH=C:/Qt/6.3.0/msvc2019_64
#	set CMAKE_BIN_PATH=C:/Qt/Tools/CMake_64/bin
#	set QTIF_BIN_DIR=C:/Qt/Tools/QtInstallerFramework/4.x/bin
#--------------------------------------------------------
#! This options only affects your current shell session, not the whole system
#--------------------------------------------------------------------------------------------------

require 'colorize'
require 'fileutils'

def showMessage(message)
    puts "------------------------------------------------------------------------------------------------------"
    puts "	#{message}"
    puts "------------------------------------------------------------------------------------------------------"
end

def showErrorMessage(message)
    puts "------------------------------------------------------------------------------------------------------".red
    puts "                                             #{message}".red
    puts "------------------------------------------------------------------------------------------------------".red
end

def executeCommand(command)
    result = %x[ #{command} ]
    showMessage(result)

    exitStatus = $?.exitstatus
    if exitStatus != 0
        showErrorMessage("Command \'#{command}\' failed!")
        exit 1
    end
    return result
end

def createDir(path)
    begin
	FileUtils.mkdir_p path	
	showMessage("Directory #{path} created")
    rescue 
	showMessage("Can't create #{path} directory")
	exit 1
    end
end

def demandEnvVars()

	showMessage("
	---------------------------------------------------------
	# Set Environment Variables like below:
	---------------------------------------------------------
	FOR MINGW COMPILER:
	Power Shell: 
		$env:Path=\"C:/Qt/Tools/mingw1120_64/bin;$env:Path\"
		$env:MINGW_BIN_DIR=\"C:/Qt/Tools/mingw1120_64/bin\"
		$env:CMAKE_BIN_PATH=\"C:/Qt/Tools/CMake_64/bin\"
		$env:CMAKE_PREFIX_PATH=\"C:/Qt/6.3.0/mingw_64\"
		$env:QTIF_BIN_DIR=\"C:/Qt/Tools/QtInstallerFramework/4.x/bin\"
		
	Cmd Shell:
		set PATH=C:/Qt/Tools/mingw1120_64/bin;%PATH%
		set MINGW_BIN_DIR=C:/Qt/Tools/mingw1120_64/bin
		set CMAKE_BIN_PATH=C:/Qt/Tools/CMake_64/bin
		set CMAKE_PREFIX_PATH=C:/Qt/6.3.0/mingw_64
		set QTIF_BIN_DIR=C:/Qt/Tools/QtInstallerFramework/4.x/bin
	---------------------------------------------------------
	FOR MSVC COMPILER:
	Power Shell:
		$env:CMAKE_PREFIX_PATH=\"C:/Qt/6.3.0/msvc2019_64\" 
		$env:CMAKE_BIN_PATH=\"C:/Qt/Tools/CMake_64/bin\"
		$env:QTIF_BIN_DIR="+"\"C:/Qt/Tools/QtInstallerFramework/4.x/bin\"
	Cmd Shell:
		set CMAKE_PREFIX_PATH=C:/Qt/6.3.0/msvc2019_64
		set CMAKE_BIN_PATH=C:/Qt/Tools/CMake_64/bin
		set QTIF_BIN_DIR=C:/Qt/Tools/QtInstallerFramework/4.x/bin
	---------------------------------------------------------
	! This options only affects your current shell session, not the whole system
	")
	exit 1
	
end

project_dir = File.expand_path('.')
build_dir = "#{project_dir}/build/windows"
deploy_dir = "#{build_dir}/Deployment"
qml_dir = "#{project_dir}/src/qml"
qmake_dir = "#{ENV['CMAKE_PREFIX_PATH']}/bin/qmake.exe"
icon_dir = "#{project_dir}/resources/barcodes-scanner.png"
dependency_dir = "#{build_dir}/dependencies"
data_dir = "#{deploy_dir}/packages/com.scythestudio.cicdsetup/data"
compiler = ""

should_build = ENV["SHOULD_BUILD"] == "true" || false
should_test = ENV["SHOULD_TEST"] == "true" || false
should_pack = ENV["SHOULD_PACK"] == "true" || false

showMessage("Windows pipeline will run with the following setup\n
Project directory #{project_dir}\n
Build directory #{build_dir}\n
Deployment directory #{deploy_dir}\n
Project's qml directory #{qml_dir}\n
Qmake directory #{qmake_dir}\n
Should build? #{should_build}\n
Should test? #{should_test}\n
Should pack? #{should_pack}\n
")

#--------------------------------------------------------------------------------------------------
# FLOW
#--------------------------------------------------------------------------------------------------

# Check if CMAKE_PREFIX_PATH is set properly
cmake_prefix_dir = ENV['CMAKE_PREFIX_PATH'] 

showMessage(cmake_prefix_dir)

if "#{cmake_prefix_dir}".include? "msvc"
	compiler = "msvc"
	showMessage("Compiler MSVC")
elsif "#{cmake_prefix_dir}".include? "mingw"
	compiler = "mingw"
	showMessage("Compiler MinGW")
	
	# Check if mingw dir set properly
	mingw_bin_dir = ENV['MINGW_BIN_DIR']
	if "#{mingw_bin_dir}".empty?
		showMessage("'MINGW_BIN_DIR' environment variable was not found!")
		demandEnvVars()
	end
else
	showMessage("Compiler path is invalid, please set CMAKE_PREFIX_PATH properly!")
	demandEnvVars()
end

# Check if cmake binary path set properly
cmake_bin_dir = ENV['CMAKE_BIN_PATH']
if "#{cmake_bin_dir}".empty?
    showMessage("'CMAKE_BIN_PATH' environment variable was not found!")
	demandEnvVars()
end

# Check if Qt Installer Framework binary path set properly
qtif_dir = ENV['QTIF_BIN_DIR']
if "#{qtif_dir}".empty?
    showMessage("QTIF_BIN_DIR' environment variable was not found!")
	demandEnvVars()
end

# Check if Qt6.3.0 installed
if not File.exist?(qmake_dir)
	showErrorMessage("Couldn't found Qt 6.3.0 installation!")
	exit 1
end

# check if Qt Framework Installer installed
if not Dir.exist?(qtif_dir)
	showErrorMessage("Couldn't found Qt Framework Installer installation!")
	exit 1
end

if should_build
	# Delete old build dir if exist
	if Dir.exist?(build_dir)
		showMessage("Build directory already exists. Removing it")
		FileUtils.rm_rf(build_dir)
	end

    FileUtils.mkdir_p(build_dir)
    if not Dir.exists?(build_dir)
        showErrorMessage("Couldn't create build folder!")
        exit 1
    end
	# Go to build dir
	FileUtils.cd(build_dir)
end

if "#{compiler}" == "mingw"
	if should_build
		showMessage("Compiling..")
		executeCommand("#{cmake_bin_dir}/cmake -G \"MinGW Makefiles\" -DCMAKE_BUILD_TYPE=Release -DCI_CD=true ../../")
		executeCommand("#{mingw_bin_dir}/mingw32-make.exe all")
		showMessage("Project built!")
	end
	target_dir = "#{build_dir}/CICDSetup.exe"
end

if "#{compiler}" == "msvc"
	if should_build
		showMessage("Compiling..")
		executeCommand("#{cmake_bin_dir}/cmake -DCMAKE_BUILD_TYPE=Release -DCI_CD=true ../../")
		executeCommand("#{cmake_bin_dir}/cmake --build . --config Release")
		showMessage("Project built!")
	end
	target_dir = "#{build_dir}/Debug/CICDSetup.exe"
end

if should_test
	FileUtils.cd("#{build_dir}/tests")
	executeCommand("ctest.exe -C Release")
end

if should_pack
	# Check executable binary file
	if !File.exist?("#{target_dir}")
		showMessage("Dynamic executable not found, run make command first!")
		exit 1
	end

	# Create dependency dir
	createDir(dependency_dir)

	# Copy executable to dependency dir
	FileUtils.cp "#{target_dir}", "#{dependency_dir}"

	# Go to dependency dir
	FileUtils.cd(dependency_dir)

	# Collect dependencieces under target dir
	showMessage("Collecting binary dependencieces")
	executeCommand("#{cmake_prefix_dir}/bin/windeployqt.exe -core -quick --qmldir #{qml_dir} ./CICDSetup.exe")

	# Create deployment dirs under build dir
	createDir(deploy_dir)
	createDir("#{deploy_dir}/config")
	createDir("#{deploy_dir}/packages")
	createDir("#{deploy_dir}/packages/com.scythestudio.cicdsetup")
	createDir("#{deploy_dir}/packages/com.scythestudio.cicdsetup/data")
	createDir("#{deploy_dir}/packages/com.scythestudio.cicdsetup/meta")

	# Copy required deployment templates from project target directory
	if File.exist?("#{project_dir}/targets/windows/config.xml")
		FileUtils.cp "#{project_dir}/targets/windows/config.xml", "#{deploy_dir}/config/config.xml"
	else
		showMessage("Could not found config.xml file under targets/windows directory!")
		exit 1
	end

	if File.exist?("#{project_dir}/targets/windows/package.xml")
		FileUtils.cp "#{project_dir}/targets/windows/package.xml", "#{deploy_dir}/packages/com.scythestudio.cicdsetup/meta/package.xml"
	else
		showMessage("Could not found package.xml file under targets/windows directory!")
		exit 1
	end

	if File.exist?("#{project_dir}/targets/windows/license.txt")
		FileUtils.cp "#{project_dir}/targets/windows/license.txt", "#{deploy_dir}/packages/com.scythestudio.cicdsetup/meta/license.txt"
	else
		showMessage("Could not found license.txt file under targets/windows directory!")
		exit 1
	end

	# Copy all dependencieces files under target dir to data dir
	showMessage("Copying data files")
	FileUtils.cp_r "#{dependency_dir}/.", data_dir

	# Go to deploy dir
	FileUtils.cd(deploy_dir)


	# Create installer
	showMessage("Creating installer..")
	executeCommand("#{qtif_dir}/binarycreator.exe -c config/config.xml -p packages CICDSampleInstaller.exe")
	
	# Pack finish message
	showMessage("Installer pack created under build/android/Deployment directory!")
end


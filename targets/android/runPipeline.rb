#--------------------------------------------------------------------------------------------------
# This script is used to generate and sign *.apk & *.aab files under build folder
#--------------------------------------------------------------------------------------------------
# To be able to create multiABI bundle follow the instructions:
#   - Download r22b version of NDK for your platform(https://github.com/android/ndk/wiki/Unsupported-Downloads)
#   - Extract the content under:
#     > C:/Utils/Android for windows
#     > /opt/android for linux
#     > for macos unknown for now
# ! When QT_ANDROID_ABIS variable passed to cmake command, multiABI will be enabled and 
#   C:/Qt/6.3.0/android_x86_64/lib/cmake/Qt6/qt.toolchain.cmake in windows or
#   /home/${USER}/Qt/6.3.0/android_x86_64/lib/cmake/Qt6/qt.toolchain.cmake file in linux will set a
#   variable to a spesific path to locate android toolchain file:
#     set(__qt_initially_configured_toolchain_file "C:/Utils/Android/android-ndk-r22b/build/cmake/android.toolchain.cmake")
#     set(__qt_initially_configured_toolchain_file "/opt/android/android-ndk-r22b/build/cmake/android.toolchain.cmake")
# That's seems weird, but instead of changing the direction of Qt source, it will be more proper to
# download the ndk and put the ndk content to desired location.
#--------------------------------------------------------------------------------------------------
# Keystore password is '6983+C2ba'
#--------------------------------------------------------------------------------------------------
# Set Environment Variables Before Running the Script
#--------------------------------------------------------------------------------------------------
# For linux:
# export CMAKE_TOOLCHAIN_FILE=/home/${USER}/Android/Sdk/ndk/22.1.7171670/build/cmake/android.toolchain.cmake (required to use NDK's CMake support)
# export CMAKE_PREFIX_PATH=/home/${USER}/Qt/6.3.0/android_armv7
# export CMAKE_BIN_PATH=/home/${USER}/Qt/Tools/CMake/bin
# export ANDROID_NDK_DIR=/home/${USER}/Android/Sdk/ndk/22.1.7171670/prebuilt/linux-x86_64/bin
# export ANDROID_SDK_ROOT=/home/${USER}/Android/Sdk
# export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
# export QT_HOST_PATH=/home/${USER}/Qt/6.3.0/gcc_64
#--------------------------------------------------------------------------------------------------
# For Windows:
# - Powershell:
	# $env:Path=\"C:/Qt/Tools/mingw1120_64/bin;$env:Path\"
	# $env:CMAKE_TOOLCHAIN_FILE=\"C:/Users/omrfr/AppData/Local/Android/Sdk/ndk/22.1.7171670/build/cmake/android.toolchain.cmake\"
	# $env:CMAKE_PREFIX_PATH=\"C:/Qt/6.3.0/android_armv7\"
	# $env:CMAKE_BIN_PATH=\"C:/Qt/Tools/CMake_64/bin\"
	# $env:ANDROID_NDK_DIR=\"C:/Users/omrfr/AppData/Local/Android/Sdk/ndk/22.1.7171670/prebuilt/windows-x86_64/bin\"
	# $env:ANDROID_SDK_ROOT=\"C:/Users/omrfr/AppData/Local/Android/Sdk\"
	# $env:JAVA_HOME=\"C:/Program Files/Android/Android Studio/jre\"
	# $env:QT_HOST_PATH=\"C:/Qt/6.3.0/mingw_64\"
# - Cmd:
	# set PATH=C:/Qt/Tools/mingw1120_64/bin;%PATH%
	# set CMAKE_TOOLCHAIN_FILE=C:/Users/omrfr/AppData/Local/Android/Sdk/ndk/22.1.7171670/build/cmake/android.toolchain.cmake
	# set CMAKE_PREFIX_PATH=C:/Qt/6.3.0/android_armv7
	# set CMAKE_BIN_PATH=C:/Qt/Tools/CMake_64/bin
	# set ANDROID_NDK_DIR=C:/Users/omrfr/AppData/Local/Android/Sdk/ndk/22.1.7171670/prebuilt/windows-x86_64/bin
	# set ANDROID_SDK_ROOT=C:/Users/omrfr/AppData/Local/Android/Sdk
	# set JAVA_HOME=C:/Program Files/Android/Android Studio/jre
	# set QT_HOST_PATH=C:/Qt/6.3.0/mingw_64
#--------------------------------------------------------------------------------------------------
# For MacOS: (NOT SURE)
# export CMAKE_TOOLCHAIN_FILE=/Users/${USER}/Android/Sdk/ndk/22.1.7171670/build/cmake/android.toolchain.cmake
# export CMAKE_PREFIX_PATH=/Users/${USER}/Qt/6.3.0/android_armv7
# export CMAKE_BIN_PATH=/Users/${USER}/Qt/Tools/CMake/bin
# export ANDROID_NDK_DIR=/Users/${USER}/Android/Sdk/ndk/22.1.7171670/prebuilt/darwin-x86_64/bin
# export ANDROID_SDK_ROOT=/Users/${USER}/Android/Sdk
# export JAVA_HOME=/Library/Java/java-11-openjdk-amd64
# export QT_HOST_PATH=/Users/${USER}/Qt/6.3.0/mingw_64
#--------------------------------------------------------------------------------------------------
#! This options only affects your current shell session, not the whole system
#--------------------------------------------------------------------------------------------------

require 'fileutils'

project_dir = File.expand_path("#{__dir__}/../../")
build_dir = "#{project_dir}/build/android"
alias_name = "scythe" 		# special for android keystore file
keyStore = "6983+C2ba"		# same w/ store pass
android_platform = "android-30"  # be sure required tools installed on your system
target_name = "CICDSetup"

should_build = ENV["SHOULD_BUILD"] == "true" || false
should_pack = ENV["SHOULD_PACK"] == "true" || false

$linux   = false
$windows = false
$macos   = false

def showMessage(message)
puts "------------------------------------------------------------------------------------------------------"
puts "    										  #{message}"
puts "------------------------------------------------------------------------------------------------------"
end

def executeCommand(command)
    result = %x[ #{command} ]
    showMessage(result)
    return result
end

def createDir(path)
    begin
	FileUtils.mkdir_p path	
	showMessage("Directory #{path} created")
    rescue 
	showMessage("Can't create #{path} directory")
	exit 1
    end
end

def detectPlatform()
	if RUBY_PLATFORM =~ /mingw/
		$windows = true
		puts '    Platform is windows!'
	elsif RUBY_PLATFORM =~ /linux/
		$linux = true
		puts '    Platform is linux!'
	elsif RUBY_PLATFORM =~ /darwin/
		$macos = true
		puts '    Platform is macos!'
	else
		puts "We're running under an unknown operating system!"
		exit 1
	end
end

def demandEnvVars()
	if $linux
		showMessage("Please enter required environment variables like below:
		export CMAKE_TOOLCHAIN_FILE=/home/${USER}/Android/Sdk/ndk/22.1.7171670/build/cmake/android.toolchain.cmake
		export CMAKE_PREFIX_PATH=/home/${USER}/Qt/6.3.0/android_armv7
		export CMAKE_BIN_PATH=/home/${USER}/Qt/Tools/CMake/bin
		export ANDROID_NDK_DIR=/home/${USER}/Android/Sdk/ndk/22.1.7171670/prebuilt/linux-x86_64/bin
		export ANDROID_SDK_ROOT=/home/${USER}/Android/Sdk
		export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
		export QT_HOST_PATH=/home/${USER}/Qt/6.3.0/gcc_64
		")
	elsif $windows
		showMessage("Please enter required environment variables like below:
		Powershell:
			$env:Path=\"C:/Qt/Tools/mingw1120_64/bin;$env:Path\"
			$env:CMAKE_TOOLCHAIN_FILE=\"C:/Users/omrfr/AppData/Local/Android/Sdk/ndk/22.1.7171670/build/cmake/android.toolchain.cmake\"
			$env:CMAKE_PREFIX_PATH=\"C:/Qt/6.3.0/android_armv7\"
			$env:CMAKE_BIN_PATH=\"C:/Qt/Tools/CMake_64/bin\"
			$env:ANDROID_NDK_DIR=\"C:/Users/omrfr/AppData/Local/Android/Sdk/ndk/22.1.7171670/prebuilt/windows-x86_64/bin\"
			$env:ANDROID_SDK_ROOT=\"C:/Users/omrfr/AppData/Local/Android/Sdk\"
			$env:JAVA_HOME=\"C:/Program Files/Android/Android Studio/jre\"
			$env:QT_HOST_PATH=\"C:/Qt/6.3.0/mingw_64\"
		Cmd:
			set PATH=C:/Qt/Tools/mingw1120_64/bin;%PATH%
			set CMAKE_TOOLCHAIN_FILE=C:/Users/omrfr/AppData/Local/Android/Sdk/ndk/22.1.7171670/build/cmake/android.toolchain.cmake
			set CMAKE_PREFIX_PATH=C:/Qt/6.3.0/android_armv7
			set CMAKE_BIN_PATH=C:/Qt/Tools/CMake_64/bin
			set ANDROID_NDK_DIR=C:/Users/omrfr/AppData/Local/Android/Sdk/ndk/22.1.7171670/prebuilt/windows-x86_64/bin
			set ANDROID_SDK_ROOT=C:/Users/omrfr/AppData/Local/Android/Sdk
			set JAVA_HOME=C:/Program Files/Android/Android Studio/jre
			set QT_HOST_PATH=C:/Qt/6.3.0/mingw_64
		")
	elsif $macos
		showMessage("Please enter required environment variables like below:
		# For MacOS: (NOT SURE)
		export CMAKE_TOOLCHAIN_FILE=/Users/${USER}/Android/Sdk/ndk/22.1.7171670/build/cmake/android.toolchain.cmake
		export CMAKE_PREFIX_PATH=/Users/${USER}/Qt/6.3.0/android_armv7
		export CMAKE_BIN_PATH=/Users/${USER}/Qt/Tools/CMake/bin
		export ANDROID_NDK_DIR=/Users/${USER}/Android/Sdk/ndk/22.1.7171670/prebuilt/darwin-x86_64/bin
		export ANDROID_SDK_ROOT=/Users/${USER}/Android/Sdk
		export JAVA_HOME=/Library/Java/java-11-openjdk-amd64
		export QT_HOST_PATH=/Users/${USER}/Qt/6.3.0/mingw_64
		")
	end
	exit 1
end

def checkFileExist(path)
	if File.exist?("#{path}")
		showMessage("#{path} found!")
	else
		showMessage("#{path} could not found!")
		exit 1
	end
end

#--------------------------------------------------------------------------------------------------
# SETUP
#--------------------------------------------------------------------------------------------------

showMessage("Android pipeline will run with the following setup\n
Project directory #{project_dir}\n
Build directory #{build_dir}\n
Android platform #{android_platform}\n
Keystore #{keyStore}\n
Alias #{alias_name}\n
Target #{target_name}\n
Should build? #{should_build}\n
Should pack? #{should_pack}\n
")


# Detect host platform
detectPlatform()

# check environment variables
cmake_toolchain_file = ENV['CMAKE_TOOLCHAIN_FILE']
if "#{cmake_toolchain_file}".empty?
    showMessage("'CMAKE_TOOLCHAIN_FILE' environment variable was not found!")
    demandEnvVars()
    exit 1
end

cmake_prefix_path = ENV['CMAKE_PREFIX_PATH']
if "#{cmake_prefix_path}".empty?
    showMessage("'CMAKE_PREFIX_PATH' environment variable was not found!")
    demandEnvVars()
end

cmake_bin_path = ENV['CMAKE_BIN_PATH']
if "#{cmake_bin_path}".empty?
    showMessage("'CMAKE_BIN_PATH' environment variable was not found!")
    demandEnvVars()
end

android_ndk_dir = ENV['ANDROID_NDK_DIR']
if "#{android_ndk_dir}".empty?
    showMessage("'ANDROID_NDK_DIR' environment variable was not found!")
    demandEnvVars()
end

android_sdk_dir = ENV['ANDROID_SDK_ROOT']
if "#{android_sdk_dir}".empty?
    showMessage("'ANDROID_SDK_ROOT' environment variable was not found!")
    demandEnvVars()
end

java_home_dir = ENV['JAVA_HOME']
if "#{java_home_dir}".empty?
    showMessage("'JAVA_HOME' environment variable was not found!")
    demandEnvVars()
end

qt_host_path = ENV['QT_HOST_PATH']
if "#{qt_host_path}".empty?
    showMessage("'QT_HOST_PATH' environment variable was not found!")
    demandEnvVars()
end

if should_build
	# Delete old build dir if exist
	begin
		if Dir.exist?(build_dir)
			showMessage("Build directory already exists. Removing it")
			FileUtils.rm_rf(build_dir)
		end
	rescue 
		showMessage("Can't remove old build directory")
		exit 1
	end
	# Create a new build dir under project dir
	createDir("#{project_dir}/build")
	createDir(build_dir)
end

# Go to build dir
FileUtils.cd(build_dir)

if $linux
	if should_build
		showMessage("Compiling..")
		executeCommand("#{cmake_bin_path}/cmake -DCMAKE_BUILD_TYPE=Release -DQT_ANDROID_ABIS=\"armeabi-v7a;arm64-v8a;x86;x86_64\" -DQT_HOST_PATH:PATH=#{qt_host_path} -DANDROID_SDK_ROOT:PATH=#{android_sdk_dir} -DCMAKE_FIND_ROOT_PATH=#{cmake_prefix_path} ../../")
		executeCommand("#{android_ndk_dir}/make -j${nproc}")
	end
	if should_pack
		showMessage("Creating app bundle..")
		executeCommand("#{qt_host_path}/bin/androiddeployqt --input ./android-"+"#{target_name}"+"-deployment-settings.json --output ./android-build --android-platform #{android_platform} --jdk #{java_home_dir} --gradle --aab --release --sign #{project_dir}/targets/android/android_release.keystore scythe --storepass #{keyStore}")
	end
elsif $windows
	if should_build
		showMessage("Compiling..")
		executeCommand("#{cmake_bin_path}/cmake -G \"MinGW Makefiles\" -DCMAKE_BUILD_TYPE=Release -DQT_ANDROID_ABIS=\"armeabi-v7a;arm64-v8a;x86;x86_64\" -DQT_HOST_PATH:PATH=#{qt_host_path} -DANDROID_SDK_ROOT:PATH=#{android_sdk_dir} -DCMAKE_FIND_ROOT_PATH=#{cmake_prefix_path} ../../")		
		executeCommand("#{android_ndk_dir}/make all")
	end
	if should_pack
		showMessage("Creating app bundle..")
		executeCommand("#{qt_host_path}/bin/androiddeployqt --input ./android-"+"#{target_name}"+"-deployment-settings.json --output ./android-build --android-platform #{android_platform} --jdk #{java_home_dir} --gradle --aab --release --sign #{project_dir}/targets/android/android_release.keystore scythe --storepass #{keyStore}")
	end
elsif $macos #(NOT TESTED)
	if should_build
		showMessage("Compiling..")
		executeCommand("#{cmake_bin_path}/cmake -DCMAKE_BUILD_TYPE=Release -DQT_ANDROID_ABIS=\"armeabi-v7a;arm64-v8a;x86;x86_64\" -DQT_HOST_PATH:PATH=#{qt_host_path} -DANDROID_SDK_ROOT:PATH=#{android_sdk_dir} -DCMAKE_FIND_ROOT_PATH=#{cmake_prefix_path} ../../")
		executeCommand("#{android_ndk_dir}/make -j${nproc}")
	end
	if should_pack
		showMessage("Creating app bundle..")
		executeCommand("#{qt_host_path}/bin/androiddeployqt --input ./android-"+"#{target_name}"+"-deployment-settings.json --output ./android-build --android-platform #{android_platform} --jdk #{java_home_dir} --gradle --aab --release --sign #{project_dir}/targets/android/android_release.keystore scythe --storepass #{keyStore}")
	end
end

if should_pack
	aab_path = "#{build_dir}/android-build/build/outputs/bundle/release/android-build-release.aab"
	# Check *.aab is exist, if so, move bundle under build directory
	checkFileExist(aab_path)
	if(File.exist?(aab_path))
		FileUtils.mv(aab_path, "#{build_dir}/#{target_name}.aab")
		showMessage("AAB file generated & signed! File -- #{build_dir}/#{target_name}.aab ")
	else
		showMessage('AAB file is missing, failed!')
		exit 1
	end
end

exit 0


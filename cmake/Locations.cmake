set(COMPANY "Scythe Studio")
set(COPYRIGHT "Copyright (c) 2022 Scythe Studio. All rights reserved.")
set(IDENTIFIER "com.PLACEHOLDER.IDENTIFIER")

# ---CONFIGURATION---
# MOST IMPORTANT PART -> change to ON/OFF if you want to use these elements in your project
#option(USE_QML "Add QML support" ON)
#option(USE_WIDGETS "Add Widgets support" ON)
# If USE_TESTS SET(ON), compile time will be increased
#option(USE_TESTS "Include tests" OFF)
#option(USE_DOCUMENTATION "Add documentation based on Doxygen template" ON)
#option(USE_TRANSLATIONS "Enable using translations" ON)
#option(USE_LIBS "Use external libraries" ON)

#option(WINDOWS_TARGET "Set Windows as the target platform" ON)
#option(LINUX_TARGET "Set Linux as the target platform" ON )
#option(MACOS_TARGET "Set MacOS as the target platform" OFF)
#option(ANDROID_TARGET "Set Android as the target platform" OFF)
#option(IOS_TARGET "Set iOS as the target platform" OFF)

#message("Support for Windows is ${WINDOWS_TARGET}")
#message("Support for Linux is ${LINUX_TARGET}")
#message("Support for macOS is ${MACOS_TARGET}")
#message("Support for Android is ${ANDROID_TARGET}")
#message("Support for iOS is ${IOS_TARGET}")

# Target specific files
set(PLATFORM_DIR "platforms")
set(WINDOWS_PLATFORM_DIR "windows")
set(LINUX_PLATFORM_DIR "linux")
set(MACOS_PLATFORM_DIR "macos")
set(ANDROID_PLATFORM_DIR "android")
set(IOS_PLATFORM_DIR "ios")

# Locations - directories in project structure
set(LIB_DIR "libs")
set(SRC_DIR "src")
set(TEST_DIR "tests")
set(DOC_DIR "documentation")
set(TRANS_DIR "translations")
set(QML_DIR "src/qml")
set(FORM_DIR "forms")
set(RES_DIR "forms")

# Check Qt version
find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Core)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Core)

# get absolute path to qmake, then use it to find windeployqt executable
get_target_property(QMAKE_EXECUTABLE Qt${QT_VERSION_MAJOR}::qmake IMPORTED_LOCATION)
get_filename_component(QT_BIN_DIR "${QMAKE_EXECUTABLE}" DIRECTORY)
